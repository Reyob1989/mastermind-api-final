package org.fboyer.mastermind.mapper;


import org.fboyer.mastermind.entity.GameEntity;
import org.fboyer.mastermind.model.Game;
import org.mapstruct.Mapper;

/**
 * Mapper for user.
 *
 * @author fboyer on 25/09/2018.
 */
@Mapper(componentModel = "spring")
public interface GameMapper extends MappableList<Game, GameEntity>{
}
