package org.fboyer.mastermind.mapper;

import java.util.Set;

/**
 * @author fboyer on 25/09/2018.
 */
public interface MappableSet<M, E> extends MappableEntity<M, E> {

	/**
	 * Maps {@link Set <E>} into a {@link Set <M>}
	 *
	 * @param entities to be mapped.
	 *
	 * @return Mapped {@link Set <M>}.
	 */
	Set<M> entitiesToModels(Set<E> entities);

	/**
	 * Maps {@link Set <M>} into a {@link Set <E>}
	 *
	 * @param models to be mapped.
	 *
	 * @return Mapped {@link Set <M>}.
	 */
	Set<E> modelsToEntities(Set<M> models);

}