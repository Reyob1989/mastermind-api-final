package org.fboyer.mastermind.controller;



import org.fboyer.mastermind.mapper.GameResponseMapper;
import org.fboyer.mastermind.model.GameResponse;
import org.fboyer.mastermind.model.constants.GameResponseUriConstants;
import org.fboyer.mastermind.service.GameResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Rest controller for Game Response
 *
 * @author fboyer on 26/09/2018.
 */
@RestController
@RequestMapping
public class GameResponseController {

    @Autowired
    private GameResponseService gameResponseService;

    @Autowired
    private GameResponseMapper gameResponseMapper;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = GameResponseUriConstants.RESPONSE, method = RequestMethod.POST)
    public GameResponse processResponse(@RequestBody final List<String> patternEntry) {
        return this.gameResponseMapper.entityToModel(this.gameResponseService.process(patternEntry));
    }
}
