package org.fboyer.mastermind.controller;



import org.fboyer.mastermind.entity.GameEntity;
import org.fboyer.mastermind.mapper.GameMapper;
import org.fboyer.mastermind.model.Game;
import org.fboyer.mastermind.model.constants.GameUriConstants;
import org.fboyer.mastermind.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Rest controller for Game
 *
 * @author fboyer on 26/09/2018.
 */
@RestController
@RequestMapping
public class GameController {

    @Autowired
    private GameService gameService;

    @Autowired
    private GameMapper gameMapper;

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = GameUriConstants.GAME, method = RequestMethod.GET)
    public Game create(@RequestBody final Game game) {
        final GameEntity gameEntity = this.gameMapper.modelToEntity(game);
        return this.gameMapper.entityToModel(this.gameService.create(gameEntity));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = GameUriConstants.HISTORIC, method = RequestMethod.GET)
    public List<Game> list() {
        return this.gameMapper.entitiesToModels(this.gameService.list());
    }
}
