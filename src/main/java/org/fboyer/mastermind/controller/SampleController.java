package org.fboyer.mastermind.controller;


import org.fboyer.mastermind.entity.SampleEntity;
import org.fboyer.mastermind.mapper.SampleMapper;
import org.fboyer.mastermind.model.Sample;
import org.fboyer.mastermind.service.SampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Sample Controller
 *
 * @author dsepulveda on 2018-09-27
 */
@RestController
public class SampleController {

	@Autowired
	private SampleService sampleService;

	@Autowired
	private SampleMapper sampleMapper;

	@RequestMapping(value = "/sample", method = RequestMethod.GET)
	public Sample get() {
		final SampleEntity entity = this.sampleService.getSample();

		final Sample result = this.sampleMapper.toModel(entity);

		return result;
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Sample create(@RequestBody Sample body) {
		final SampleEntity entity = this.sampleMapper.toEntity(body);

		final SampleEntity resultEntity = this.sampleService.create(entity);

		final Sample result = this.sampleMapper.toModel(resultEntity);

		return result;
	}

	@RequestMapping(value = "/{sampleId}", method = RequestMethod.GET)
	public Sample getById(@PathVariable final Long sampleId) {
		final SampleEntity entity = this.sampleService.get(sampleId);

		final Sample result = this.sampleMapper.toModel(entity);

		return result;
	}

}
