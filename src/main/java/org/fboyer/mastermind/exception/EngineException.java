package org.fboyer.mastermind.exception;

import org.fboyer.mastermind.model.enumeration.ErrorTypeEnum;

/**
 * Base exception implementation to handle errors in the API
 *
 * @author fboyer on 25/09/2018.
 */
public class EngineException extends RuntimeException {

    private static final long serialVersionUID = 168838955720897919L;

    protected ErrorTypeEnum errorType;
    protected Object[] messageParameters;

    /**
     * Creates a new instance of {@link EngineException} with the given message
     *
     * @param message error message
     */
    public EngineException(String message) {
        super(message);
    }

    /**
     * Creates a new instance of {@link EngineException} with the given message and exception object
     *
     * @param message   error message
     * @param exception thrown exception
     */
    public EngineException(String message, Throwable exception) {
        super(message, exception);
    }

    /**
     * Creates a new instance of {@link EngineException} with the given exception object
     *
     * @param exception thrown exception
     */
    public EngineException(Throwable exception) {
        super(exception);
    }

    /**
     * Creates a new instance of {@link EngineException} with the given error type and message
     *
     * @param errorType error type
     * @param message   error message
     */
    public EngineException(ErrorTypeEnum errorType, String message) {
        this(message);
        this.errorType = errorType;
    }

    /**
     * Creates a new instance of {@link EngineException} with the given error type, the message and the parameters for the user friendly message
     *
     * @param errorType         error type
     * @param message           error message
     * @param messageParameters parameters to use for the i18n of the user friendly message
     */
    public EngineException(ErrorTypeEnum errorType, String message, Object[] messageParameters) { // NOPMD
        this(errorType, message);
        this.messageParameters = messageParameters; // NOPMD
    }

    /**
     * Creates a new instance of {@link EngineException} with the given error type, message and exception object
     *
     * @param errorType error type
     * @param message   error message
     * @param exception thrown exception
     */
    public EngineException(ErrorTypeEnum errorType, String message, Throwable exception) {
        this(message, exception);
        this.errorType = errorType;
    }

    /**
     * Creates a new instance of {@link EngineException} with the given error type, message, exception object and the parameters for the user friendly message
     *
     * @param errorType         error type
     * @param message           error message
     * @param exception         thrown exception
     * @param messageParameters parameters to use for the i18n of the user friendly message
     */
    public EngineException(ErrorTypeEnum errorType, String message, Throwable exception, Object[] messageParameters) { // NOPMD
        this(errorType, message, exception);
        this.messageParameters = messageParameters; // NOPMD
    }

    /**
     * Creates a new instance of {@link EngineException} with the given error type, and exception object
     *
     * @param errorType error type
     * @param exception thrown exception
     */
    public EngineException(ErrorTypeEnum errorType, Throwable exception) {
        this(exception);
        this.errorType = errorType;
    }

    /**
     * Creates a new instance of {@link EngineException} with the given error type, exception object and the parameters for the user friendly message
     *
     * @param errorType         error type
     * @param exception         thrown exception
     * @param messageParameters parameters to use for the i18n of the user friendly message
     */
    public EngineException(ErrorTypeEnum errorType, Throwable exception, Object[] messageParameters) { // NOPMD
        this(errorType, exception);
        this.messageParameters = messageParameters; // NOPMD
    }

    /**
     * Returns the error type of the exception
     *
     * @return
     */
    public ErrorTypeEnum getErrorType() {
        return errorType;
    }

    /**
     * Returns the parameters to use for the internationalization o the user friendly message
     *
     * @return
     */
    public Object[] getMessageParameters() {
        return messageParameters; // NOPMD
    }
}
