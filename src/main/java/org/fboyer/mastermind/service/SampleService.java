package org.fboyer.mastermind.service;

import org.fboyer.mastermind.entity.SampleEntity;

/**
 * Sample Service Interface
 *
 * @author dsepulveda on 2018-09-27
 */
public interface SampleService {

	SampleEntity getSample();

	SampleEntity create(SampleEntity entity);

	SampleEntity get(Long sampleId);
}
