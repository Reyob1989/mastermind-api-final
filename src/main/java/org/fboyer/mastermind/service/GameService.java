package org.fboyer.mastermind.service;

import org.fboyer.mastermind.entity.GameEntity;

import java.util.List;

/**
 * Game Service interface.
 *
 * @author fboyer on 25/09/2018
 */
public interface GameService {

	/**
	 * Get the GameEntity based on its identifier.
	 *
	 * @param gameId     Game identifier.
	 *
	 * @return Obtained GameEntity
	 */
	GameEntity get(final Long gameId);

	/**
	 * Get the latest GameEntity.
	 **
	 * @return Obtained GameEntity
	 */
	GameEntity getLastGameActive();

	/**
	 * Create the Game in the database.
	 *
	 * @param gameEntity Game.
	 *
	 * @return Created  GameEntity
	 */
	GameEntity create(final GameEntity gameEntity);

    /**
     * Delete the GameEntity based on its identifier.
     *
     * @param gameId  Game identifier.
     */
    void delete(final Long gameId);

	/**
	 * Get all GameEntities in database.
	 **
	 * @return List<GameEntity> Countries.
	 */
	List<GameEntity> list();

	/**
	 * Get all GameEntities in database filter by user name.
	 *
	 * @param name  Name filter.
	 *
	 * @return List<GameEntity> Countries.
	 */
	List<GameEntity> listFilterByName(final String name);
}
