package org.fboyer.mastermind.service;


import org.fboyer.mastermind.entity.GameResponseEntity;

import java.util.List;

/**
 * Game Response Service interface.
 *
 * @author fboyer on 25/09/2018
 */
public interface GameResponseService {

	/**
	 * Get the GameEntity based on its identifier.
	 *
	 * @return Obtained GameEntity
	 */
	GameResponseEntity process(final List<String> patternEntry);
}
