package org.fboyer.mastermind.service.impl;


import org.fboyer.mastermind.entity.SampleEntity;
import org.fboyer.mastermind.repository.SampleRepository;
import org.fboyer.mastermind.service.SampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementation for {@link SampleService}
 *
 * @author dsepulveda on 2018-09-27
 */
@Service
public class SampleServiceImpl implements SampleService {

	@Autowired
	private SampleRepository sampleRepository;

	@Override
	public SampleEntity getSample() {
		final SampleEntity result = new SampleEntity();
		result.setSampleId(1L);
		result.setProperty1("Patata");
		result.setProperty2(123L);
		return result;
	}

	@Override
	public SampleEntity create(final SampleEntity entity) {
		entity.setSampleId(null);
		return this.sampleRepository.save(entity);
	}

	@Override
	public SampleEntity get(final Long sampleId) {
		return this.sampleRepository.findById(sampleId).get();
	}
}
