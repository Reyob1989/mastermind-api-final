package org.fboyer.mastermind.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Sample Entity class
 *
 * @author dsepulveda on 2018-09-27
 */
@Entity
public class SampleEntity implements Serializable {

	private static final long serialVersionUID = -422724441008078892L;

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Long sampleId;

	private String property1;
	private Long property2;

	public Long getSampleId() {
		return this.sampleId;
	}

	public void setSampleId(final Long sampleId) {
		this.sampleId = sampleId;
	}

	public String getProperty1() {
		return this.property1;
	}

	public void setProperty1(final String property1) {
		this.property1 = property1;
	}

	public Long getProperty2() {
		return this.property2;
	}

	public void setProperty2(final Long property2) {
		this.property2 = property2;
	}
}
