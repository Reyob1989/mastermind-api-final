package org.fboyer.mastermind.entity;

import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * Auditable entity for database record
 *
 * @author fboyer on 25/09/2018.
 */
@MappedSuperclass
public class AuditableEntity implements Serializable {

    private static final long serialVersionUID = 7639472339629033924L;

    private String createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedAt;

	public String getCreatedBy() {
	    return this.createdBy;
    }

	public void setCreatedBy(final String createdBy) {
		if (this.createdBy == null) {
            this.createdBy = createdBy;
        }
    }

    public Date getCreatedAt() {
	    return this.createdAt;
    }

	public void setCreatedAt(final Date createdAt) {
		if (this.createdAt == null) {
            this.createdAt = createdAt;
        }
    }

    public Date getModifiedAt() {
	    return this.modifiedAt;
    }

	public void setModifiedAt(final Date modifiedAt) {
		this.modifiedAt = modifiedAt;
    }

    @PrePersist
    protected void setDefaultsOnCreate() {
	    this.createdBy = "system";
	    final Date currentDate = new Date();
	    this.createdAt = currentDate;
	    this.modifiedAt = currentDate;
    }

    @PreUpdate
    protected void setDefaultsOnUpdate() {
	    this.modifiedAt = new Date();
    }
}
