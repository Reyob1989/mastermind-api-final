package org.fboyer.mastermind.controller;

import org.fboyer.mastermind.model.Sample;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URL;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Test class for {@link SampleController}
 *
 * @author dsepulveda on 2018-09-27
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GameControllerTestIT {

	@LocalServerPort
	private int port;

	private URL base;

	@Autowired
	private TestRestTemplate template;

	@Before
	public void setUp() throws Exception {
		this.base = new URL("http://localhost:" + this.port + "/");
	}

	@Test
	public void getHello() throws Exception {
		final ResponseEntity<Sample> response = this.template.getForEntity(this.base.toString() + "sample",
				Sample.class);

		assertThat(response.getBody(), is(notNullValue()));
		assertThat(response.getBody().getSampleId(), is(1L));
		assertThat(response.getBody().getProperty1(), is("Patata"));
		assertThat(response.getBody().getProperty2(), is(123L));
	}

	@Test
	public void testCreateAndGet() throws Exception {
		final Sample modelToCreate = new Sample();
		modelToCreate.setProperty1("First");
		modelToCreate.setProperty2(11L);

		final ResponseEntity<Sample> responseCreate = this.template.postForEntity(
				this.base.toString(), modelToCreate, Sample.class);

		assertThat(responseCreate.getBody(), is(notNullValue()));
		assertThat(responseCreate.getBody().getSampleId(), is(greaterThanOrEqualTo(1L)));
		assertThat(responseCreate.getBody().getProperty1(), is("First"));
		assertThat(responseCreate.getBody().getProperty2(), is(11L));

		final String uriGet = UriComponentsBuilder.fromHttpUrl(this.base.toString()+ "{sampleId}")
				.buildAndExpand(responseCreate.getBody().getSampleId()).toString();


		final ResponseEntity<Sample> response = this.template.getForEntity(
				uriGet, Sample.class);

		assertThat(response.getBody(), is(notNullValue()));
		assertThat(response.getBody().getSampleId(), is(responseCreate.getBody().getSampleId()));
		assertThat(response.getBody().getProperty1(), is("First"));
		assertThat(response.getBody().getProperty2(), is(11L));
	}
}
