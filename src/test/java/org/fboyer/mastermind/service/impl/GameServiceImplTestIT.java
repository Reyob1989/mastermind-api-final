package org.fboyer.mastermind.service.impl;

import org.fboyer.mastermind.entity.GameEntity;
import org.fboyer.mastermind.exception.EngineException;
import org.fboyer.mastermind.model.enumeration.ErrorTypeEnum;
import org.fboyer.mastermind.service.GameService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GameServiceImplTestIT {

	@Autowired
	private GameService gameService;

	@Test
	public void GivedValidGame_WhenCallCreate_ThenMustCreateGame(){
		final String userName = "name";
		final String pattern = "color,color,color,color";

		final GameEntity gameEntity = new GameEntity();
		gameEntity.setCodeMakerName(userName);
		gameEntity.setPattern(pattern);

		final GameEntity gameEntityCreated = this.gameService.create(gameEntity);

		Assert.assertEquals(userName, gameEntityCreated.getCodeMakerName());
		Assert.assertEquals(pattern, gameEntityCreated.getPattern());
	}

	@Test
	public void GivedInValidGameName_WhenCallCreate_ThenMustThrowErrow(){
		final String userName = "";
		final String pattern = "color,color,color,color";

		final GameEntity gameEntity = new GameEntity();
		gameEntity.setCodeMakerName(userName);
		gameEntity.setPattern(pattern);

		try {
			this.gameService.create(gameEntity);
		} catch (final EngineException e){
			Assert.assertEquals(ErrorTypeEnum.MISSING_PARAMETER,e.getErrorType());
		}
	}

	@Test
	public void GivedInValidGamePattern_WhenCallCreate_ThenMustThrowErrow(){
		final String userName = "name";
		final String pattern = "color,color,color";

		final GameEntity gameEntity = new GameEntity();
		gameEntity.setCodeMakerName(userName);
		gameEntity.setPattern(pattern);

		try {
			this.gameService.create(gameEntity);
		} catch (final EngineException e){
			Assert.assertEquals(ErrorTypeEnum.GAME_NOT_VALID,e.getErrorType());
		}
	}

	@Test
	public void GivedValidGame_WhenCallGet_ThenMustGetCreateGame(){
		final String userName = "name";
		final String pattern = "color,color,color,color";

		final GameEntity gameEntity = new GameEntity();
		gameEntity.setCodeMakerName(userName);
		gameEntity.setPattern(pattern);

		this.gameService.create(gameEntity);

		final GameEntity gameEntitySaved = this.gameService.get(1L);

		Assert.assertEquals(userName, gameEntitySaved.getCodeMakerName());
		Assert.assertEquals(pattern, gameEntitySaved.getPattern());
	}

	@Test
	public void GivedValidGame_WhenCallGet_ThenMustThrowError(){

		try {
			this.gameService.get(null);
		} catch (final EngineException e){
			Assert.assertEquals(ErrorTypeEnum.MISSING_PARAMETER,e.getErrorType());
		}
	}
}